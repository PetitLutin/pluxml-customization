# PluXml core customizations

## WARNING - DISCLAIMER
These modifications are made to fit **my** needs, It is not recommended to make modifications of PluXml's core because they will be lost at the next updates. I'm not responsible if you reproduce what I did and if it breaks your site!

Plus, I'm on a PluXml 5.8.9 and PluXml 6 will come soon, and it may be really different from previous versions, so once again, be careful.